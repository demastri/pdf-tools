﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using JPD.PDFToolsLib.Data;
using JPD.PDFToolsLib.Core;

namespace NewPDFClient
{
    class Program
    {
        static void Main(string[] args)
        {
            string engine = "itextsharp";
            bool isDev = true;
            string ProjectPath = AppDomain.CurrentDomain.BaseDirectory;
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(ProjectPath);
            if (isDev)
                di = di.Parent.Parent;

            Project thisProject = new Project("FirstProject", 
                System.IO.Path.Combine( di.FullName, "PDFToolsData") );

            thisProject.SetEngine(engine);
            DataContext dc = new DataContext();
            dc.Add("TestTemplateDE1", DataElement.Load("TestDE", null));
            dc.Add("TestTemplateDE2", DataElement.Load("TestDE", "SomeContext"));

            PDFResult pdf1 = thisProject.Render(dc);
            if (pdf1.IsSuccess())
                pdf1.SaveTo(System.IO.Path.Combine(di.FullName, "PDFToolsData", "Output", "SomeOutFile.pdf"));
            else
            {

                dc = new DataContext();
                dc.Add("TestTemplateDE1", DataElement.Load("TestDE", null));
                dc.Add("TestTemplateDE2", DataElement.Load("TestDE", "SomeOtherContext"));

                PDFResult pdf2 = thisProject.Render(dc);
            }
        }
    }
}
