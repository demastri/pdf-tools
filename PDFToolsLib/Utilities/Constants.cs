﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using JPD.PDFToolsLib.Utilities;

namespace JPD.PDFToolsLib
{
    public class Constants
    {
        public static PDFColor White = new PDFColor("00000000");
        public static PDFColor Black = new PDFColor("FFFFFFFF");
        public static PDFColor Red = new PDFColor("00E8C200");
        public static PDFColor Blue = new PDFColor("FFFF0000");
        public static PDFColor Green = new PDFColor("FF00FF00");
        public static PDFColor BorderGrey = new PDFColor("19191919");

        public static Dictionary<string, PDFColor> ColorsByName = new Dictionary<string,PDFColor>() { 
            {"White", White}, {"Black", Black}, {"Red", Red}, {"Blue", Blue}, {"Green",Green}, {"BorderGrey", BorderGrey} };

        //////////////////////////////////////////

        internal static ElementSize [] emptyMargins = {-1, -1, -1, -1};
        internal static RectangleF emptyBounds = new Rectangle(-1, -1, -1, -1);
        internal static PointF emptyPoint = new Point(-1, -1);
        
        internal const int Top = 0;
        internal const int Bottom = 1;
        internal const int Left = 2;
        internal const int Right = 3;

        public enum PaperType { Undefined = -1,                                    Letter = 0, Legal }
        static internal List<ElementSize> PaperWidths = new List<ElementSize>() {  "8.5in",    "8.5in" };
        static internal List<ElementSize> PaperHeights = new List<ElementSize>() { "11in",     "14in" };

        public enum Orientation
        {
            Undefined = -1,

            Portrait = 0,
            Landscape = 1
        }

        static internal T GetEnumVal<T>(string s, T defValue) where T : struct, IComparable
        {
            T outVal;
            if (Enum.TryParse(s, out outVal))
                return outVal;
            return defValue;
        }
        


        internal static byte[] StringToBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
