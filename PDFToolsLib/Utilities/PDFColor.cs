﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JPD.PDFToolsLib.Utilities
{
    /// want to be able to say things like:
    /// Color x = new Color( Color.Black );
    /// assert (x.RGB.blue == 255);
    /// assert (x.CMYK.green == 0);

    public class PDFColor
    {
        public RGBColor RGB;
        public CMYKColor CMYK;

        public PDFColor( PDFColor c ) 
        {
            RGB = new RGBColor(c.RGB.red, c.RGB.blue, c.RGB.green);
            CMYK = new CMYKColor(RGB);
        }
        // the assumption is that this is a hex string, 6 char for rgb, 8 for cmyk
        public PDFColor( string s )
        {
            if (s.Length == 6)
            {
                RGB = new RGBColor(
                    DecodeHexString(s.Substring(0, 2)),
                    DecodeHexString(s.Substring(2, 2)),
                    DecodeHexString(s.Substring(4, 2)) );
                CMYK = new CMYKColor(RGB);
            }
            if (s.Length == 8)
            {
                int testVal = DecodeHexString(s.Substring(0, 2));
                RGB = new RGBColor(
                    DecodeHexString(s.Substring(0, 2)),
                    DecodeHexString(s.Substring(2, 2)),
                    DecodeHexString(s.Substring(4, 2)),
                    DecodeHexString(s.Substring(6, 2)));
                CMYK = new CMYKColor(RGB);
            }
        }
        public PDFColor(int r, int g, int b)
        {
            RGB = new RGBColor(r, g, b);
            CMYK = new CMYKColor(RGB);
        }
        public PDFColor(int c, int m, int y, int k)
        {
            int refVal = c + k;
            RGB.red = 255 - (refVal < 255 ? refVal : 255);

            refVal = m + k;
            RGB.green = 255 - (refVal < 255 ? refVal : 255);

            refVal = y + k;
            RGB.blue = 255 - (refVal < 255 ? refVal : 255);

            CMYK = new CMYKColor(RGB);
        }

        private int DecodeHexString(string s)
        {
            string sparse = s.Trim().ToUpper();
            return 16 * DecodeHexChar(s[0]) + DecodeHexChar(s[1]);
        }
        private int DecodeHexChar(char c)
        {
            if( '0' <= c && c <= '9' )
                return (c-'0');
            if( 'A' <= c && c <= 'F' )
                return (c - 'A' + 10);
            return -1;
        }
        
        /////////////////////

        public struct RGBColor
        {
            public int red;
            public int green;
            public int blue;

            public RGBColor(int r, int g, int b)
            {
                red = r;
                green = g;
                blue = b;
            }

            public RGBColor(int c, int m, int y, int k)
            {
                int refVal = c + k;
                red = 255 - (refVal < 255 ? refVal : 255);

                refVal = m + k;
                green = 255 - (refVal < 255 ? refVal : 255);

                refVal = y + k;
                blue = 255 - (refVal < 255 ? refVal : 255);
            }
        }
        public struct CMYKColor
        {
            public int cyan;
            public int magenta;
            public int yellow;
            public int black;

            public CMYKColor(RGBColor rgb)
            {
                int maxVal = (rgb.red > rgb.green ? rgb.red : rgb.green);
                maxVal = (maxVal > rgb.blue ? maxVal : rgb.blue);
                black = 255 - maxVal;
                if (black == 255)
                    cyan = magenta = yellow = 0;
                else
                {
                    cyan = 255 * (255 - rgb.red - black) / (255 - black);
                    magenta = 255 * (255 - rgb.green - black) / (255 - black);
                    yellow = 255 * (255 - rgb.blue - black) / (255 - black);
                }
            }
        }
    }
}
