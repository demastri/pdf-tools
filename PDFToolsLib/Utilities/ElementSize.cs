﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JPD.PDFToolsLib.Utilities
{
    internal enum LengthUnits
    {
        Points,
        Picas,
        Inches,
        Millimeters
    }

    public class ElementSize
    {
        internal ElementSize()
        {
            points = 0;
        }
        internal ElementSize(double pts)
        {
            points = pts;
        }
        internal ElementSize(string value)
        {
            if (value.Contains("in"))
                ins = Convert.ToDouble( value.Substring( 0, value.IndexOf("in") ) );
            else if (value.Contains("mm"))
                mms = Convert.ToDouble(value.Substring(0, value.IndexOf("mm")));
            else if (value.Contains("pc"))
                picas = Convert.ToDouble(value.Substring(0, value.IndexOf("pc")));
            else if (value.Contains("pt"))
                points = Convert.ToDouble(value.Substring(0, value.IndexOf("pt")));
            else
                points = Convert.ToDouble(value);
        }
        internal ElementSize(double value, LengthUnits unit)
        {
            switch (unit)
            {
                case LengthUnits.Points:
                    points = value;
                    break;
                case LengthUnits.Picas:
                    picas = value;
                    break;
                case LengthUnits.Inches:
                    ins = value;
                    break;
                case LengthUnits.Millimeters:
                    mms = value;
                    break;

            }
        }
        private double sizeInPoints;
        internal double points { get { return sizeInPoints; } set { sizeInPoints = value; } }
        internal double picas { get { return sizeInPoints / 12.0; } set { points = (value * 12.0); } }
        internal double ins { get { return points / 72.0; } set { points = (value * 72.0); } }
        internal double mms { get { return ins * 25.4; } set { ins = value / 25.4; } }

        public static ElementSize operator +(ElementSize lhs, ElementSize rhs) { return new ElementSize(lhs.points + rhs.points); }
        public static ElementSize operator *(int lhs, ElementSize rhs) { return new ElementSize(lhs * rhs.points); }
        public static implicit operator ElementSize(double i) { return new ElementSize(i); }
        public static implicit operator ElementSize(string s) { return new ElementSize(s); }
        public static implicit operator double(ElementSize s) { return s.points; }
        public static implicit operator float(ElementSize s) { return (float)s.points; }
    }
}
