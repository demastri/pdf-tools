﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Threading.Tasks;

namespace JPD.PDFToolsLib.Utilities
{
    class XMLHelpers
    {
        internal static string FindXMLFile(string subFolder, string xpath)
        {
            string basePath = PDFToolsLib.Core.Project.BaseProjectFilePath;

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Path.Combine(basePath, subFolder));
            System.IO.FileInfo[] fia = di.GetFiles("*.xml");
            foreach (System.IO.FileInfo fi in fia)
            {
                XDocument f = XDocument.Load(fi.FullName);
                if (f.XPathSelectElements(xpath).Count() > 0) //if we're here, this document is correct
                    return fi.FullName;
            }
            return "";
        }

        internal static string GetSafeAttribute(XElement xElt, string attrName, string defValue)
        {
            if (xElt.Attribute(attrName) == null)
                return defValue;
            return xElt.Attribute(attrName).Value;
        }
        internal static List<string> GetSafeAttributeList(XElement xElt, string attrName, string defValue)
        {
            List<string> outList = new List<string>();
            string processString = (xElt.Attribute(attrName) == null ? defValue : xElt.Attribute(attrName).Value);
            if (processString.Trim() != "")
                foreach (string elt in processString.Split(','))
                {
                    outList.Add(elt);
                }
            return outList;
        }

    }
}
