﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using JPD.PDFToolsLib;
using JPD.PDFToolsLib.Data;

namespace JPD.PDFToolsLib.Data
{
    public class DataContext
    {
        Dictionary<string, DataContext> children;
        Dictionary<string, DataElement> leaves;


        public DataContext()
        {
            children = new Dictionary<string, DataContext>();
            leaves = new Dictionary<string, DataElement>();
        }
        public void Add(string tag, string s)
        {
            Add(tag, new DataElement(s));
        }
        public void Add(string tag, DataElement de)
        {
            leaves.Add(tag, de);
        }
        public void Add(string tag, DataContext dc)
        {
            children.Add(tag, dc);
        }
    }
}
