﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JPD.PDFToolsLib.Data
{
    public class DataElement
    {
        string internalValue;

        public DataElement()
        {
            BaseInit("");
        }
        public DataElement(string val)
        {
            BaseInit(val);
        }
        private void BaseInit(string s)
        {
            internalValue = s;
        }

        /// <summary>
        /// DataElement is the abstraction over any arbitrary representation of data
        /// 
        /// there are two major ways that data is stored:
        /// dictionary data - element returned given a key value
        /// tabular data  - element returned based on "row/col" data
        /// 
        /// usage
        ///  1-project defines replaceable tags
        ///  2-instantiation defines actual sources for data contexts
        ///  3-data context loads these elements and maintains the mappings between elements and replaceable 
        ///  4-data elements are that values returned from the is project specific detail that includes 
        ///
        /// note that as long as an element can be returned, we can keep this as a relatively consistent interface
        /// 
        /// data = dc["name"].AsInt()
        /// data = dc["name"]["row"].Value() // explicit request for data value as string
        /// data = dc["name"]["row"].AsString() // equivalent to above
        /// data = dc["name"]["row"].Format()
        /// data = dc["name"]["row"]["col"].AsDouble()
        /// 
        /// As long 
        /// 
        /// typical use cases:
        /// 
        /// global report parameter
        ///  Definition in the report - <Data DC=Global DE=Date />
        ///  Definition in instantiation - DE.Add["Global"]["Date"] = Today.ToString();
        /// 
        /// simple page level override
        ///  Definition in the report - <Data DC=PageData.Page2 DE=Footer />
        ///  Definition in instantiation - 
        ///     DE.Add["PageData"]["Page1"]["Footer"] = SomeText
        ///     DE.Add["PageData"]["Page2"]["Footer"] = SomeOtherText
        /// 
        /// text from xml
        /// 
        /// text from a doc
        /// 
        /// table data - sql, excel, csv, xml
        /// 
        /// chart data - sql, excel, csv, xml
        /// 
        /// 
        /// 
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        static public DataElement Load(string tag, string context)
        {

            return new DataElement();
        }
    }
}
