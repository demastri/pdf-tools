﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Threading.Tasks;
using JPD.PDFToolsLib;
using JPD.PDFToolsLib.Data;
using JPD.PDFToolsLib.Utilities;
using JPD.PDFToolsLib.RenderEngines;

namespace JPD.PDFToolsLib.Core
{
    public class Project
    {
        /// <summary>
        /// A project is simply a collection of Page objects that want to be rendered.
        /// It has a name and potentially a Guid ID, and default wireframe and style set can be defined for it
        /// 
        /// It can basically be given a data context and rendered, that's about it.  
        /// If it's successful, the stream can be saved as a file 
        /// </summary>
        /// 

        public static string BaseProjectFilePath = "";
        
        Engine renderEngine;

        List<Page> pages;
        Page projOverride;  // override for wireframe details and stylesets

        Guid ID;
        string name;

        public Project()
        {
            pages = new List<Page>();
        }
        public Project(string projectName, string basePath)
        {
            name = projectName;
            ID = Guid.Empty;
            pages = new List<Page>();

            BaseProjectFilePath = basePath;

            Init(name);
        }
        public Project(Guid projectID)
        {
            Init(ID);
        }

        public bool SetEngine(string engineName)
        {
            renderEngine = Engine.Factory(engineName);
            return renderEngine == null;
        }
        public PDFResult Render(DataContext dc)
        {
            // for each page
            //  render that page
            //   why so much harder than that??

            // managing the actual output document is the only thing the render engine 
            // needs to worry about - everything else should happen through abstractions
            Document curDoc = renderEngine.CreateDocument();

            Page defaultPageStyles = BuildDefaultPageStyle();
            
            curDoc.SetStatus( true );
            curDoc.Open();
            foreach (Page pg in pages)
                if (!pg.Render(curDoc, dc, defaultPageStyles))
                    curDoc.SetStatus(false);
            curDoc.Close();
            PDFResult outResult = curDoc.GetResults();

            return outResult;
        }

        private Page BuildDefaultPageStyle()
        {
            /// load the default wireframe and styleset for this project
            /// add it to a placeholder page, and return it
            return new Page();
        }
        private void Init(string projectName)  // xml?
        {
            pages = new List<Page>();
            projOverride = new Page();
            string fullPath = XMLHelpers.FindXMLFile("Projects", "/Project[@Name='" + name + "']");
            if (fullPath != "")
            {
                XDocument f = XDocument.Load(fullPath);
                ID = new Guid(XMLHelpers.GetSafeAttribute(f.Root, "ID", Guid.Empty.ToString()));

                projOverride = new Page(f.Root, null);
                
                foreach (XElement xe in f.XPathSelectElements("/Project/Pages/Page"))
                    pages.Add(new Page(xe, projOverride));
            }
            name = projectName;
            ID = Guid.Empty;
        }
        private void Init(Guid projectID)      // sql or other binary format?
        {
            name = projectID.ToString();
            ID = projectID;
            pages = new List<Page>();

        }
    
    }
}
