﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace JPD.PDFToolsLib.Core
{
    public class PDFResult
    {
        internal bool success;
        internal byte[] outContent;

        public bool IsSuccess()
        {
            return success;
        }
        public void SaveTo(string fullPath)
        {
            FileStream fs = System.IO.File.OpenWrite(fullPath);

            fs.Write(outContent, 0, outContent.Length);

            fs.Flush();
            fs.Close();
        }

    }
}
