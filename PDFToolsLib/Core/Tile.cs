﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using JPD.PDFToolsLib.Utilities;
using JPD.PDFToolsLib.Data;
using JPD.PDFToolsLib.RenderEngines;

namespace JPD.PDFToolsLib.Core
{
    public class Tile
    {
        internal DisplayElements.DisplayElement dispElt;
        internal PointF HeightRanges;
        internal ElementSize RenderedHeight;
        internal Point GridLocation;
        internal int columnSpan;
        private StyleSet styles;

        internal Tile()
        {
            styles = new StyleSet();
            HeightRanges = new PointF(-1, -1);
            GridLocation = new Point(-1, -1);

            RenderedHeight = 0;
        }
        internal Tile(XElement xe)
        {
            XElement deElt = xe.Element("Display");
            List<string> gridList = XMLHelpers.GetSafeAttributeList(deElt, "Grid", "1,1");

            GridLocation = new Point((gridList.Count > 0 ? Convert.ToInt32(gridList[0]) : 0), (gridList.Count > 1 ? Convert.ToInt32(gridList[1]) : 0));
            columnSpan = Convert.ToInt32(XMLHelpers.GetSafeAttribute(deElt, "ColumnSpan", "1"));
            string deType = XMLHelpers.GetSafeAttribute(deElt, "Type", "");
            List<string> hts = XMLHelpers.GetSafeAttributeList(deElt, "HeightRange", "1,1");
            HeightRanges = new PointF(new ElementSize(hts[0]), new ElementSize(hts[1]));


            dispElt = DisplayElements.DisplayElement.DisplayElementFactory(deType);

            XElement styleElt = xe.Element("Style");
            styles = new StyleSet(styleElt);
        }

        internal RectangleF Render(Canvas c, DataContext dc, StyleSet defaultStyles)
        {
            styles.Merge(defaultStyles);
            RectangleF outRect = dispElt.Render(c, dc, styles);
            RenderedHeight = outRect.Height;
            return outRect;

        }

    }
}
