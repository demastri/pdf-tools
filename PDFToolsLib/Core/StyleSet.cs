﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Threading.Tasks;

namespace JPD.PDFToolsLib
{
    class StyleSet
    {
        Dictionary<string, string> styleDict;
        internal StyleSet()
        {
        }
        internal StyleSet(XElement ssElt)
        {
            styleDict = new Dictionary<string, string>();
            foreach (XAttribute attr in ssElt.Attributes())
            {
                styleDict.Add(attr.Name.LocalName, attr.Value);
            }
        }

        internal string this[string key]
        {
            get
            {
                if (styleDict.ContainsKey(key))
                    return styleDict[key];
                return "";
            }
        }

        internal void Merge(StyleSet rhs)
        {
            // ###
        }
    }
}
