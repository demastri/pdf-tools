﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Threading.Tasks;
using JPD.PDFToolsLib.Utilities;

namespace JPD.PDFToolsLib.Core
{
    internal class Container
    {
        internal string name;
        bool anchored;
        internal int columnSpan;
        internal int startCol;
        internal PointF heightRanges;

        internal Container(XElement thisNode)
        {
            Init(thisNode);
        }
        internal Container(string thisName)
        {
            Init(thisName);
        }

        void Init(XElement thisNode)
        {
            name = XMLHelpers.GetSafeAttribute(thisNode, "Name", "");
            anchored = (XMLHelpers.GetSafeAttribute(thisNode, "Anchored", "false").Trim().ToLower() == "true");
            columnSpan = Convert.ToInt32(XMLHelpers.GetSafeAttribute(thisNode, "ColumnSpan", "1"));
            startCol = Convert.ToInt32(XMLHelpers.GetSafeAttribute(thisNode, "StartColumn", "1"));
            List<string> hts = XMLHelpers.GetSafeAttributeList(thisNode, "HeightRange", "1,1");
            heightRanges = new PointF(new ElementSize(hts[0]), new ElementSize(hts[1]));
        }
        void Init(string thisName)
        {
            name = thisName;
            anchored = true;
            columnSpan = 1;
        }
        internal Point LocateTileWidth(Tile t)
        {
            int localStartCol = t.GridLocation.Y;
            int localEndCol = localStartCol + t.columnSpan - 1;
            int ctrEndCol = startCol + columnSpan - 1;
            if (localStartCol > columnSpan)
                localStartCol = columnSpan;
            if (localEndCol > columnSpan)
                localEndCol = columnSpan;
            int localColSpan = localEndCol - localStartCol + 1;

            return new Point(localStartCol, localColSpan);
        }


    }
}
