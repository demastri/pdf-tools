﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using JPD.PDFToolsLib.RenderEngines;
using JPD.PDFToolsLib.Utilities;
using JPD.PDFToolsLib.Data;

namespace JPD.PDFToolsLib.Core
{
    public class Page
    {
        Dictionary<Container, List<Tile>> tiles;
        Wireframe wireframe;
        StyleSet styles;
        int absoluteRenderNumber;

        private List<ElementSize> currentDrawnSpace;

        internal Page()
        {
            absoluteRenderNumber = 1;
            wireframe = new Wireframe();
            styles = new StyleSet();
            tiles = new Dictionary<Container, List<Tile>>();
        }
        internal Page(XElement xe, Page projOverride)
        {
            absoluteRenderNumber = 1;
            styles = new StyleSet();
            tiles = new Dictionary<Container, List<Tile>>();
            /// Page elements have 2 potential initializers: template, wireframe
            ///     templates are used to define common component layouts (not yet implemented)
            ///     wireframes provide default values for paper, orientation, margins, column widths, and galley
            ///         the wireframe attribute on the page node points to an external reference wireframe definition
            ///     page definitions can include a wireframe child node to override any values in the reference wf
            ///     page definitions can include a styles child node to set style defaults
            if (xe.Attribute("Template") != null)
            {
                // ### deal with you later...
            }

            // the override hierarchy is general wireframe styles, overridden by project defaults, overridden by page defaults...
            // wireframes have to be complete, but a downstream entity can provide only what it wants to override
            // ### requires nullable entries (if I didn't provide a "Margins" entry, don't override it...

            // 1 - this merges in the project specific overrides handed to us when we started (will also be null for a project default call)
            StartWithProjectDefaults(projOverride);
            // 0 - this pulls the base from the wireframe named in this page definition - will be null if we're setting up a project default
            OverrideWithPageNamedWireframe(xe);
            // 2 - and this handles the specific overrides included in the page or project definition
            OverrideWithExplicitPageWireframeNode(xe);

            wireframe.CalculatePageDimensions();
            wireframe.InitColumnWidths();

            BuildTileContainerMapping(xe);
        }
        private void StartWithProjectDefaults(Page projOverride)
        {
            if (wireframe == null)
                wireframe = new Wireframe();
            if (projOverride != null && projOverride.wireframe != null)
                wireframe.MergeGaps(projOverride.wireframe);

            if (styles == null)
                styles = new StyleSet();
            if (projOverride != null && projOverride.styles != null)
                styles.Merge(projOverride.styles);
        }

        private void OverrideWithPageNamedWireframe(XElement xe)
        {
            Wireframe pgNodeWF = (xe.Attribute("Wireframe") != null ? new Wireframe(xe.Attribute("Wireframe").Value) : null);
            if (pgNodeWF != null)
                wireframe.MergeGaps(pgNodeWF);
        }
        private void OverrideWithExplicitPageWireframeNode(XElement xe)
        {
            foreach (XElement wfElt in xe.Elements("Wireframe"))
            {
                Wireframe overrideWF = new Wireframe(wfElt);
                wireframe = overrideWF.MergeGaps(wireframe);
            }
            foreach (XElement ssElt in xe.Elements("Style"))
            {
                StyleSet overrideSS = new StyleSet(ssElt);
                styles.Merge(overrideSS);
            }
        }

        private void BuildTileContainerMapping(XElement xe)
        {
            foreach (XElement tElt in xe.Elements("Tile"))
            {
                string ctrName = XMLHelpers.GetSafeAttribute(tElt.Element("Display"), "Container", "-Page-");
                Container thisContainer = wireframe.FindContainer(ctrName);
                if (thisContainer != null)  // otherwise error...
                {
                    if (!tiles.ContainsKey(thisContainer))
                        tiles.Add(thisContainer, new List<Tile>());
                    tiles[thisContainer].Add(new Tile(tElt));
                }
            }
        }

        internal bool Render(Document doc, DataContext dc, Page defaultProjPage)
        {
            Canvas c = doc.AddPage(wireframe.paper, wireframe.orientation);
            styles.Merge(defaultProjPage.styles);
            foreach (Container ctr in tiles.Keys)
            {
                InitLayout(ctr);
                SortedDictionary<long, Tile> tList = RenderOrderedTiles(tiles[ctr]);
                foreach (long tKey in tList.Keys)
                {
                    // note!! all of the values contained in these Rectangles are ints representing length as PDF points
                    Tile renderTile = tList[tKey];

                    // LocateTileWidth returns the container cols this will render into
                    // Layout returns the actual location on the page for the maximum size that this tile can draw into
                    RectangleF reservedLoc = Layout(ctr, ctr.LocateTileWidth(renderTile), renderTile.HeightRanges.Y);

                    // this verifies that we have a place to draw
                    if (reservedLoc.Equals(Constants.emptyBounds))
                        return false;

                    // this defines the only place the tile can draw into
                    c.SetDrawingWindow(reservedLoc);
                    // this returns the space the tile actually used
                    RectangleF actBound = renderTile.Render(c, dc, styles);

                    // if drawing fails, the tile will report that it used no space
                    if (actBound.Equals(Constants.emptyBounds))
                        return false;

                    // manage the available space on the page, given what the tile actually used
                    UpdateLayout(ctr, ctr.LocateTileWidth(renderTile), reservedLoc.Top + renderTile.RenderedHeight);
                }
            }

            return true;
        }

        /// <summary>
        /// within a container, we need to use our best current judgment on how / where tiles will render
        /// the algorithm is simple:
        ///     determine a render order based on grid location - 10000 * row (x, first coord) + 100 * col (y, 2nd), + tie offset in lexical order
        ///     initialize each column's current written height
        ///     foreach tile in render order prior
        ///         find it's current render height - either its actual render height or its max height
        /// 
        /// </summary>
        /// <param name="maxBound"></param>
        /// <param name="gridLoc"></param>
        /// <returns></returns>
        private void InitLayout(Container ctr)
        {
            currentDrawnSpace = new List<ElementSize>();
            for (int i = 0; i < wireframe.colWidths.Count; i++)
                currentDrawnSpace.Add(wireframe.margins[Constants.Top]);
        }

        private SortedDictionary<long, Tile> RenderOrderedTiles(List<Tile> tList)
        {
            SortedDictionary<long, Tile> outDict = new SortedDictionary<long, Tile>();
            foreach (Tile t in tList)
            {
                long tileScore = 10000 * t.GridLocation.X + 100 * t.GridLocation.Y;
                while (outDict.ContainsKey(tileScore))
                    tileScore++;
                outDict.Add(tileScore, t);
            }
            return outDict;
        }

        private RectangleF Layout(Container ctr, Point ctrColumns, ElementSize height)
        {
            int absStartCol = ctr.startCol + ctrColumns.X - 1;
            ElementSize width = new ElementSize(0);
            ElementSize deepestCol = new ElementSize();
            // width = column widths + galleys, height = maxheight
            for (int i = 0; i < ctrColumns.Y; i++)
            {
                if (currentDrawnSpace[i + absStartCol - 1] > deepestCol)
                    deepestCol = currentDrawnSpace[i + absStartCol - 1];
                width += wireframe.colWidths[i + absStartCol-1] + wireframe.galley;
            }
            width -= wireframe.galley;

            ElementSize offset = wireframe.margins[Constants.Left];
            if (absoluteRenderNumber % 2 == 1)
                offset += wireframe.binding;
            for (int i = 1; i < absStartCol; i++)
                offset += wireframe.colWidths[i - 1] + wireframe.galley;

            if (height + deepestCol > ctr.heightRanges.Y)
                height = ctr.heightRanges.Y;

            return new RectangleF(offset, deepestCol, width, height);
        }

        private void UpdateLayout(Container ctr, Point ctrColumns, ElementSize height)
        {
            int absStartCol = ctr.startCol + ctrColumns.X - 1;
            // width = column widths + galleys, height = maxheight
            for (int i = 0; i < ctrColumns.Y; i++)
            {
                currentDrawnSpace[i + absStartCol - 1] = height;
            }

        }
    }
}
