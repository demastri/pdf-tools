﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Threading.Tasks;
using JPD.PDFToolsLib.Utilities;

namespace JPD.PDFToolsLib.Core
{
    class Wireframe
    {
        string name;
        internal Constants.PaperType paper;
        internal Constants.Orientation orientation;
        ElementSize width;
        ElementSize height;

        internal ElementSize[] margins = Constants.emptyMargins;   // these values are in points, t,b,l,r
        internal ElementSize galley;                       // width between cols
        internal ElementSize binding;                      // width to offset binding space - at left for odd pages, right for even
        internal List<ElementSize> colWidths;
        int nbrColumns { get { return colWidths.Count; } }

        List<Container> containers;

        internal Wireframe()
        {
            Init();
        }
        // copy constructor
        internal Wireframe(Wireframe rhs)
        {
            paper = rhs.paper;
            orientation = rhs.orientation;
            CalculatePageDimensions();
            margins[Constants.Top] = rhs.margins[Constants.Top];
            margins[Constants.Bottom] = rhs.margins[Constants.Bottom];
            margins[Constants.Left] = rhs.margins[Constants.Left];
            margins[Constants.Right] = rhs.margins[Constants.Right];
            galley = rhs.galley;
            binding = rhs.binding;

            colWidths = new List<ElementSize>();
            foreach (ElementSize es in rhs.colWidths)
                colWidths.Add(es);
        }
        internal Wireframe(string wfName)
        {
            string wfFullPath = XMLHelpers.FindXMLFile("Wireframes", "/Wireframe[@Name='" + wfName + "']");
            if (wfFullPath != "")
            {
                XDocument f = XDocument.Load(wfFullPath);
                Init(f.Root, true);
            }
            else
                Init();

        }
        internal Wireframe(XElement wfElt)
        {
            Init(wfElt, false);
        }

        private void Init()
        {
            paper = Constants.PaperType.Undefined;
            orientation = Constants.Orientation.Undefined;
            margins = Constants.emptyMargins;
            galley = -1;
            binding = -1;
            colWidths = new List<ElementSize>();
            colWidths.Add(new ElementSize(1, LengthUnits.Inches));   // 1 col
        }
        private void Init(XElement wfElt, bool setDefaults)
        {
            name = XMLHelpers.GetSafeAttribute(wfElt, "Name", "");

            paper = Constants.GetEnumVal<Constants.PaperType>(XMLHelpers.GetSafeAttribute(wfElt, "Paper", ""),
                setDefaults ? Constants.PaperType.Letter : Constants.PaperType.Undefined);

            orientation = Constants.GetEnumVal<Constants.Orientation>(XMLHelpers.GetSafeAttribute(wfElt, "Orientation", ""),
                setDefaults ? Constants.Orientation.Portrait : Constants.Orientation.Undefined);

            CalculatePageDimensions();

            InitMargins(XMLHelpers.GetSafeAttributeList(wfElt, "Margins", "0"), setDefaults);

            galley = Convert.ToInt32(XMLHelpers.GetSafeAttribute(wfElt, "Galley", setDefaults ? "7" : "-1"));     // default of 1/10"

            binding = Convert.ToInt32(XMLHelpers.GetSafeAttribute(wfElt, "Binding", setDefaults ? "0" : "-1"));     // default of no binding offset

            colWidths = new List<ElementSize>();

            colWidths = new List<ElementSize>();
            List<string> cwList = XMLHelpers.GetSafeAttributeList(wfElt, "ColumnWidths", setDefaults ? "-1" : "");
            foreach (ElementSize es in cwList)
                colWidths.Add(es);

            containers = new List<Container>();
            containers.Add(new Container("-Page-"));
            foreach (XElement xctr in wfElt.Elements("Container"))
            {
                containers.Add(new Container(xctr));
            }
        }

        private void InitMargins(List<string> s, bool setDefaults)
        {
            switch (s.Count)
            {
                case 0: // all the same
                    margins[Constants.Top] = margins[Constants.Bottom] = margins[Constants.Left] = margins[Constants.Right] =
                        setDefaults ? new ElementSize(0.5, LengthUnits.Inches) : new ElementSize(-1);
                    break;
                case 1: // all the same
                    margins[Constants.Top] = margins[Constants.Bottom] = margins[Constants.Left] = margins[Constants.Right] = s[0];
                    break;
                case 2: // top/bottom, then l/r
                    margins[Constants.Top] = margins[Constants.Bottom] = s[0];
                    margins[Constants.Left] = margins[Constants.Right] = s[1];
                    break;
                case 3: // top, bottom, then l/r
                    margins[Constants.Top] = s[0];
                    margins[Constants.Bottom] = s[1];
                    margins[Constants.Left] = margins[Constants.Right] = s[2];
                    break;
                default:    // at least 4 available...
                    margins[Constants.Top] = s[0];
                    margins[Constants.Bottom] = s[1];
                    margins[Constants.Left] = s[2];
                    margins[Constants.Right] = s[3];
                    break;
            }
        }

        internal void InitColumnWidths()
        {
            CalculatePageDimensions();

            int defaultCount = 0;
            ElementSize totalUsedWidth = 0;
            foreach (ElementSize es in colWidths)
            {
                if (es == -1)
                    defaultCount++;
                else
                    totalUsedWidth += es;
            }

            if (defaultCount > 0 && paper != Constants.PaperType.Undefined) // leave as default marker if we don't have a page
            {
                totalUsedWidth += margins[Constants.Left] + (nbrColumns - 1) * galley + margins[Constants.Right] + binding;

                ElementSize defaultColWidth = (width - totalUsedWidth) / defaultCount;
                if (defaultColWidth < 0)
                    defaultColWidth = 0;

                for (int i = 0; i < nbrColumns; i++)
                    if (colWidths[i] == -1)
                        colWidths[i] = defaultColWidth;
            }
        }

        internal void CalculatePageDimensions()
        {
            if (paper == Constants.PaperType.Undefined)
                width = height = -1;
            else
            {
                width = (orientation == Constants.Orientation.Portrait ? Constants.PaperWidths[(int)paper] : Constants.PaperHeights[(int)paper]);
                height = (orientation == Constants.Orientation.Portrait ? Constants.PaperHeights[(int)paper] : Constants.PaperWidths[(int)paper]);
            }
        }

        internal Wireframe MergeGaps(Wireframe rhs)
        {
            if (paper == Constants.PaperType.Undefined)
                paper = rhs.paper;

            if (orientation == Constants.Orientation.Undefined)
                orientation = rhs.orientation;

            if (margins == Constants.emptyMargins && rhs.margins != Constants.emptyMargins)  // ok, there's margin data here
            {
                margins[Constants.Top] = rhs.margins[Constants.Top];
                margins[Constants.Bottom] = rhs.margins[Constants.Bottom];
                margins[Constants.Left] = rhs.margins[Constants.Left];
                margins[Constants.Right] = rhs.margins[Constants.Right];
            }

            if (galley == -1)
                galley = rhs.galley;

            if (binding == -1)
                binding = rhs.binding;

            if (colWidths.Count == 0)
            {
                colWidths.Clear();
                foreach (ElementSize es in rhs.colWidths)
                    colWidths.Add(es);
            }

            if (containers == null || containers.Count == 1)
                containers = rhs.containers;
            return this;
        }
        internal Container FindContainer(string name)
        {
            foreach (Container ctr in containers)
                if (ctr.name == name)
                    return ctr;
            return null;
        }
    }
}
