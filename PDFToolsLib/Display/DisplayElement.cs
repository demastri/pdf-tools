﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using JPD.PDFToolsLib.Utilities;
using JPD.PDFToolsLib.Core;
using JPD.PDFToolsLib.Data;
using JPD.PDFToolsLib.RenderEngines;

namespace JPD.PDFToolsLib.DisplayElements
{
    abstract class DisplayElement
    {
        protected string baseXmlString;
        string name;

        internal static DisplayElement DisplayElementFactory(string deType)
        {
            switch (deType)
            {
                case "ImageBox":
                    return new SampleDisplayElement(deType);
                default:
                    return new SampleDisplayElement(deType);
            }
        }

        internal DisplayElement(XElement xe)
        {
            baseXmlString = xe.ToString();
            name = XMLHelpers.GetSafeAttribute(xe, "Type", "");
        }
        internal DisplayElement(string s)
        {
            baseXmlString = "";
            name = s;
        }

        internal RectangleF Render(Canvas c, DataContext dc, StyleSet defaultStyles)
        {
            RectangleF myCanvas = c.GetDrawingWindow();

            ResolveDataContext();

            ResolveStyles();

            myCanvas = RenderElements(c);
            c.DrawBorder(defaultStyles["Border"]);

            return myCanvas;
        }

        internal virtual void ResolveDataContext()
        {
        }

        internal virtual void ResolveStyles()
        {
        }

        abstract internal RectangleF RenderElements(Canvas c);

    }
}
