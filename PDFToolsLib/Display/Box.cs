﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using JPD.PDFToolsLib.Core;
using JPD.PDFToolsLib.RenderEngines;

namespace JPD.PDFToolsLib.DisplayElements
{
    class Box : DisplayElement
    {
        internal Box(string s)
            : base(s)
        {
        }
        internal Box(XElement xe)
            : base(xe)
        {
        }

        override internal RectangleF RenderElements( Canvas c )
        {
            RectangleF box = c.GetDrawingWindow();

            c.Rectangle(Constants.Blue, 2.0, box);
            
            return c.GetDrawingWindow();
        }

    }
}
