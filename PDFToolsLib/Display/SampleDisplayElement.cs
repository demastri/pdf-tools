﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using JPD.PDFToolsLib.Core;
using JPD.PDFToolsLib.RenderEngines;

namespace JPD.PDFToolsLib.DisplayElements
{
    class SampleDisplayElement : DisplayElement
    {
        internal SampleDisplayElement(string s)
            : base(s)
        {
        }
        internal SampleDisplayElement(XElement xe)
            : base(xe)
        {
        }

        override internal void ResolveDataContext()
        {
        }

        override internal void ResolveStyles()
        {
        }

        override internal RectangleF RenderElements( Canvas c )
        {
            return c.GetDrawingWindow();
        }

    }
}
