﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JPD.PDFToolsLib;

namespace JPD.PDFToolsLib.RenderEngines
{
    class Engine
    {
        internal static Engine Factory(string engineName)
        {
            switch (engineName.ToLower().Trim() )
            {
                case "itextsharp":
                    return new itext.Engine();
            }
            return null;
        }

        internal virtual Document CreateDocument()
        {
            return null;
        }
        internal virtual Canvas CreateCanvas()
        {
            return null;
        }
    }
}
