﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using JPD.PDFToolsLib.Core;
using JPD.PDFToolsLib.IPDFRender;
using iTextSharp;

namespace JPD.PDFToolsLib.RenderEngines 
{
    public class Document
    {
        public PDFResult GetResults()
        {
            PDFResult outResult = new PDFResult();
            outResult.success = isSuccess;
            outResult.outContent = ToBytes();

            return outResult;
        }
        public bool Status()
        {
            return isSuccess;
        }
        public void SetStatus(bool s)
        {
            isSuccess = s;
        }

        // proxy the IDocument interface
        public Canvas AddPage(Constants.PaperType paper, Constants.Orientation rotation)
        {
            engineDoc.AddPage(paper, rotation);

            docCanvas.Rectangle(Constants.Blue, 2.0, new System.Drawing.Rectangle(10, 10, 10, 10));
            List<System.Drawing.PointF> v = new List<System.Drawing.PointF>();
            v.Add(new System.Drawing.PointF(5, 0));
            v.Add(new System.Drawing.PointF(10, 5));
            v.Add(new System.Drawing.PointF(5, 10));
            v.Add(new System.Drawing.PointF(0, 5));
            docCanvas.DrawPolygon(v, false);

            return docCanvas;
        }

        public bool IsOpen()
        {
            return engineDoc.IsOpen();
        }

        public void Open()
        {
            engineDoc.Open();
        }

        public void Close()
        {
            engineDoc.Close();
        }

        public byte[] ToBytes()
        {
            return outStream.ToArray();
        }

        /// this is the in-memory representation of the rendered document
        /// there will likely be handles to actual render element details
        /// 
        internal Document(MemoryStream s, IDocument engDoc)
        {
            Init(s, engDoc);
        }

        private bool isSuccess;
        private IDocument engineDoc;
        private Canvas docCanvas;
        private MemoryStream outStream;

        private void Init(MemoryStream s, IDocument engDoc)
        {
            outStream = s;
            engineDoc = engDoc;
            docCanvas = new Canvas(engineDoc.GetCanvas());
        }

    }
}
