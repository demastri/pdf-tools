﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using JPD.PDFToolsLib.Utilities;
using JPD.PDFToolsLib.IPDFRender;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace JPD.PDFToolsLib.RenderEngines.itext
{
    public class engCanvas : ICanvas
    {
        RectangleF drawBounds;
        internal Point paperBounds;

        public engCanvas()
        {
            SetDrawingWindow(Constants.emptyBounds);
        }
        public RectangleF GetDrawingWindow()
        {
            return drawBounds;
        }

        /// this is the plac on the page that a given tile is currently allowed to draw into...
        public void SetDrawingWindow(RectangleF r)
        {
            drawBounds = r;
        }

        public void Open()
        {
            engineTemplate = engineCanvas.DirectContent;
        }
        public void Flush()
        {
            engineCanvas.Flush();
        }
        public void Close()
        {
        }

        public void SetFillColor(PDFColor c)
        {
        }
        public void SetStrokeColor(PDFColor c)
        {
            engineTemplate.SetCMYKColorStroke(c.CMYK.cyan, c.CMYK.magenta, c.CMYK.yellow, c.CMYK.black);
        }
        public void SetBackgroundColor(PDFColor c)
        {
        }
        public void SetTextColor(PDFColor c)
        {
        }
        public void Fill(PointF p)
        {
        }

        public void SetLineWeight(ElementSize weight)
        {
            engineTemplate.SetLineWidth(weight);
        }
        public void WriteText(PointF loc, string text) 
        {
        }
        public void DrawLine(PointF start, PointF end)
        {
            PointF sPt = Translate(start);
            PointF ePt = Translate(end);
            engineTemplate.MoveTo(sPt.X, sPt.Y);
            engineTemplate.LineTo(ePt.X, ePt.Y);
            engineTemplate.Stroke();
        }

        /// <summary>
        /// internal representation below (includes public initializer)
        /// </summary>

        internal engCanvas(iTextSharp.text.Document engineDoc, System.IO.Stream outStream)
        {
            engineCanvas = iTextSharp.text.pdf.PdfWriter.GetInstance(engineDoc, outStream);
            engineTemplate = null;
        }

        private iTextSharp.text.pdf.PdfWriter engineCanvas;
        private iTextSharp.text.pdf.PdfContentByte engineTemplate;

        private System.Drawing.Rectangle Translate(System.Drawing.Rectangle genericRect)
        {
            return genericRect;
        }
        private PointF Translate(PointF genericPt)
        {

            return new PointF((GetDrawingWindow().X + genericPt.X), paperBounds.Y - (GetDrawingWindow().Y + genericPt.Y));
        }

    }
}
