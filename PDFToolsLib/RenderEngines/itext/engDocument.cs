﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using JPD.PDFToolsLib.IPDFRender;
using iTextSharp;

namespace JPD.PDFToolsLib.RenderEngines.itext 
{
    public class engDocument : IDocument
    {
        static iTextSharp.text.Rectangle[] localPageSizes = 
        {
            iTextSharp.text.PageSize.LETTER,
            iTextSharp.text.PageSize.LEGAL
        };

        // IDocument interface
        public ICanvas AddPage(Constants.PaperType paper, Constants.Orientation rotation)
        {
            engineDoc.SetPageSize(rotation == Constants.Orientation.Landscape ? localPageSizes[(int)paper].Rotate() : localPageSizes[(int)paper]);
            engineDoc.NewPage();

            engineCanvas.paperBounds = new System.Drawing.Point((int)engineDoc.PageSize.Width, (int)engineDoc.PageSize.Height);

            return engineCanvas;
        }

        public bool IsOpen()
        {
            return engineDoc.IsOpen();
        }

        public void Open()
        {
            engineDoc.Open();
            engineCanvas.Open();
        }

        public void Close()
        {
            engineCanvas.Flush();
            engineDoc.Close();
        }

        public ICanvas GetCanvas()
        {
            return engineCanvas;
        }


        /// this is the in-memory representation of the rendered document
        /// there will likely be handles to actual render element details
        /// 
        internal engDocument(Stream s)
        {
            Init(s);
        }

        private iTextSharp.text.Document engineDoc;
        private engCanvas engineCanvas;

        private void Init(Stream s)
        {
            engineDoc = new iTextSharp.text.Document();
            engineCanvas = new engCanvas(engineDoc, s);

        /// from the other world
        /// doc = new doc()
        /// writer = new writer( doc new stream )
        /// doc.setpagesize
        /// writer.closestream = true
        /// doc.open
        /// projbyte = writer.dc
        
        }

    }
}
