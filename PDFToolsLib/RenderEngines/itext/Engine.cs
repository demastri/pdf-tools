﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp;

namespace JPD.PDFToolsLib.RenderEngines.itext 
{
    internal class Engine : JPD.PDFToolsLib.RenderEngines.Engine
    {
        override internal Document CreateDocument()
        {
            System.IO.MemoryStream s = new System.IO.MemoryStream();
            Document outDoc = new Document( s, new engDocument(s) );
            return outDoc;
        }
    }
}
