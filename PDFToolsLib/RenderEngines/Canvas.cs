﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using JPD.PDFToolsLib.Utilities;
using JPD.PDFToolsLib.IPDFRender;

namespace JPD.PDFToolsLib.RenderEngines
{
    public class Canvas
    {
        /// anything implmented in the base should be platform independent
        /// the ICanvas interface should do the actual drawing
        /// DisplayElements should actually write to a RenderCanvas item
        /// individual render engines 
        /// 

        private ICanvas engineCanvas;

        public Canvas(ICanvas engCanvas)
        {
            engineCanvas = engCanvas;
            LineWeight = 3;
        }

        public PDFColor BackgroundColor { get; set; }
        public PDFColor TextColor { get; set; }
        public PDFColor StrokeColor { get; set; }
        public PDFColor FillColor { get; set; }
        public ElementSize LineWeight { get; set; }

        /// this draws a box with lines centered on the vertex points in r
        /// relies on the DrawLine primitive
        ///     does NOT modify the stroke color or thickness
        ///     does NOT currently respect the Drawing Window
        /// signatures available to specify only the rectangle, the rect and color, or the rect, color and thickness and rotation
        /// the given rectangle defines the space in which the rotated rectangle will be inscribed
        ///     this respects the aspect ratio of the original rectangle
        #region Rectantle
        public void Rectangle(RectangleF r)
        {
            Rectangle(StrokeColor, LineWeight, r);
        }
        public void Rectangle(PDFColor c, RectangleF r)
        {
            Rectangle(c, LineWeight, r);
        }
        public void Rectangle(PDFColor c, ElementSize thickness, RectangleF r)
        {
            Rectangle(c, thickness, r, 0.0);
        }
        public void Rectangle(PDFColor c, ElementSize thickness, RectangleF r, double rotation)
        {
            // for each of the four lines, extend each edge in the same direction along the line 1/2 of the line thickness
            // and draw the line...
            List<PointF> v = new List<PointF>();
            v.Add(new PointF(r.Left, r.Top));
            v.Add(new PointF(r.Right, r.Top));
            v.Add(new PointF(r.Right, r.Bottom));
            v.Add(new PointF(r.Left, r.Bottom));

            StrokeColor = c;
            LineWeight = thickness;
            DrawPolygon(v, false);

        }
        #endregion

        // this draws a box with lines on the interior of vertex points
        public void DrawBorder(string detail)
        {
            RectangleF baseRect = GetDrawingWindow();
            string[] param = detail.Split(',');
            if (param.Count() == 2 && Constants.ColorsByName.ContainsKey(param[0]))
            {
                ElementSize localWt = new ElementSize(param[1]);
                Rectangle(
                        Constants.ColorsByName[param[0]],
                        param[1],
                        new RectangleF(
                            baseRect.X + localWt / 2.0f, baseRect.Y + localWt / 2.0f,
                            baseRect.Width - localWt, baseRect.Height - localWt));
            }
        }

        // ### need to ectend lines so that endpoints meet...
        // ### the point here is that the endcaps don't ma
        public void DrawPolygon(List<PointF> vertex, bool fill)
        {
            int ct = vertex.Count;
            for( int i=0; i<ct; i++ )
            {
                PointF prior = vertex[(i - 1 + ct) % ct];
                PointF start = vertex[(i + 0 + ct) % ct];
                PointF end   = vertex[(i + 1 + ct) % ct];
                PointF next  = vertex[(i + 2 + ct) % ct];

                System.Diagnostics.Debug.Print("prior< " + prior.X.ToString() + ", " + prior.Y.ToString() + ">");
                System.Diagnostics.Debug.Print("start< " + start.X.ToString() + ", " + start.Y.ToString() + ">");
                System.Diagnostics.Debug.Print("end< " + end.X.ToString() + ", " + end.Y.ToString() + ">");
                System.Diagnostics.Debug.Print("next< " + next.X.ToString() + ", " + next.Y.ToString() + ">");

                if (start != PointF.Empty && end != PointF.Empty && start != end)
                {
                    double startAngle = VertexAngle(prior, start, end);
                    double endAngle = VertexAngle(start, end, next);

                    double lineAngle = VertexAngle(end, start, new PointF(start.X + 100, start.Y));
                    double dx = (start.X - end.X);
                    double dy = (start.Y - end.Y);
                    double m = dy / dx;

                    System.Diagnostics.Debug.Print("startAngle " + startAngle.ToString());
                    System.Diagnostics.Debug.Print("endAngle " + endAngle.ToString());
                    System.Diagnostics.Debug.Print("lineAngle" + lineAngle.ToString());

                    if (startAngle >= Math.PI/2.0)
                    {
                        double adv = LineWeight / 2.0 * Math.Tan(startAngle / 2) / Math.Sqrt(dx * dx + dy * dy);
                        double incrX = adv * Math.Abs(dx);
                        double incrY = adv * Math.Abs(dy);
                        start.Y += (float)(start.Y >= end.Y ? incrY : -incrY);
                        start.X += (float)(start.X >= end.X ? incrX : -incrX);
                    }
                    if (endAngle >= Math.PI / 2.0)
                    {
                        double adv = LineWeight / 2.0 * Math.Tan(endAngle / 2) / Math.Sqrt(dx * dx + dy * dy);
                        double incrX = adv * Math.Abs(dx);
                        double incrY = adv * Math.Abs(dy);
                        end.Y += (float)(start.Y >= end.Y ? -incrY : incrY);
                        end.X += (float)(start.X >= end.X ? -incrX : incrX);
                    }
                    DrawLine(start, end);

#if false
            for (int i = 0; i < 4; i++)
            {
                PointF start = v[i];
                PointF end = v[i + 1];
                double incrX;
                double incrY;
                if (start.X == end.X)
                {
                    incrX = 0;
                    incrY = 0.5 * thickness;
                }
                else
                {
                    double dx = (start.X - end.X);
                    double dy = (start.Y - end.Y);
                    double m = dy / dx;
                    double factor = thickness * 0.5 / Math.Sqrt(dx * dx + dy * dy);
                    incrX = factor * Math.Abs(dx);
                    incrY = factor * Math.Abs(dy);

                }
                start.Y += (float)(start.Y > end.Y ? incrY : -incrY);
                end.Y += (float)(start.Y > end.Y ? -incrY : incrY); ;
                start.X += (float)(start.X > end.X ? incrX : -incrX);
                end.X += (float)(start.X > end.X ? -incrX : incrX); ;

                DrawLine(c, thickness, start, end);
            }
#endif
                }
            }

            if (fill)
            {
                SetFillColor();
                Fill(FindInteriorPoint(vertex));
            }
        }
        private double VertexAngle(PointF l, PointF v, PointF r) 
        {
            int lQuadrant = (l.X >= v.X ? (l.Y >= v.Y ? 1 : 4) : (l.Y >= v.Y ? 2 : 3));
            double absLAngle = Math.Atan((l.Y - v.Y) / (l.X - v.X));
            if (lQuadrant == 2 || lQuadrant == 3)
                absLAngle += Math.PI;
            int rQuadrant = (r.X >= v.X ? (r.Y >= v.Y ? 1 : 4) : (r.Y >= v.Y ? 2 : 3));
            double absRAngle = Math.Atan((r.Y - v.Y) / (r.X - v.X));
            if (rQuadrant == 2 || rQuadrant == 3)
                absRAngle += Math.PI;
            return Math.Abs(absLAngle - absRAngle);
        }

        private PointF FindInteriorPoint(List<PointF> vertex)
        {
            return PointF.Empty;
        }

        // primitives we expect engines to implement
        public void SetDrawingWindow(RectangleF r)
        {
            engineCanvas.SetDrawingWindow(r);
        }
        public RectangleF GetDrawingWindow()
        {
            RectangleF dw = engineCanvas.GetDrawingWindow();
            dw.X = dw.Y = 1;
            return dw;
        }
        public void SetBackgroundColor()
        {
            engineCanvas.SetBackgroundColor(BackgroundColor);
        }
        public void SetTextColor()
        {
            engineCanvas.SetTextColor(TextColor);
        }
        public void SetStrokeColor()
        {
            engineCanvas.SetStrokeColor(StrokeColor);
        }
        public void SetFillColor()
        {
            engineCanvas.SetFillColor(FillColor);
        }
        public void Fill(PointF p)
        {
            engineCanvas.Fill(p);
        }

        public void SetLineWeight()
        {
            engineCanvas.SetLineWeight(LineWeight);
        }
        public void WriteText(Point loc, string text)
        {
            engineCanvas.WriteText(loc, text);
        }
        public void DrawLine(PointF start, PointF end)
        {
            DrawLine(StrokeColor, LineWeight, start, end);
        }
        public void DrawLine(PDFColor c, PointF start, PointF end)
        {
            DrawLine(c, LineWeight, start, end);
        }
        public void DrawLine(PDFColor c, ElementSize thickness, PointF start, PointF end)
        {
            ElementSize oldLW = LineWeight;
            PDFColor oldStroke = StrokeColor;
            LineWeight = thickness;
            StrokeColor = c;

            SetLineWeight();
            SetStrokeColor();
            engineCanvas.DrawLine(start, end);

            LineWeight = oldLW;
            StrokeColor = oldStroke;
        }

    }
}
