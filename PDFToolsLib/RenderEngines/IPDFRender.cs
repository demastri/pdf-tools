﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using JPD.PDFToolsLib.Core;
using JPD.PDFToolsLib.Utilities;

namespace JPD.PDFToolsLib.IPDFRender
{
    /// <summary>
    /// the goal of this interface is to wrap any particular implementation of PDF rendering into a set of
    /// calls that can be easily changed for another engine.  May require some ridiculous mapping, but there's 
    /// probably some utility for this down the road
    /// 
    /// To be clear - no tile or displayelement render code should call a render engine directly, everything 
    /// should run through this interface, properly implemented
    /// </summary>
    interface IEngine
    {
        IDocument CreateDocument();
    }
    public interface IDocument
    {
        ICanvas AddPage(Constants.PaperType paper, Constants.Orientation rotation);
        bool IsOpen();
        void Open();
        void Close();
        ICanvas GetCanvas();
    }
    public interface ICanvas
    {
        // establish the range a tile can draw into
        void SetDrawingWindow(RectangleF r);
        RectangleF GetDrawingWindow();

        // primitives we expect engines to implement
        void SetBackgroundColor(PDFColor c);
        void SetTextColor(PDFColor c);
        void SetStrokeColor(PDFColor c);
        void SetFillColor( PDFColor c );
        void Fill(PointF p);

        void SetLineWeight(Utilities.ElementSize weight);
        void WriteText(PointF loc, string text);
        void DrawLine(PointF start, PointF end);
    }
}

